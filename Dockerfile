FROM eclipse-temurin:17
RUN mkdir /app
ARG JAR_VERSION
COPY /target/Modelisation-3d-du-corps-humain-${JAR_VERSION}.jar /app/app.jar
ENTRYPOINT ["java", "-jar", "/app/app.jar"]