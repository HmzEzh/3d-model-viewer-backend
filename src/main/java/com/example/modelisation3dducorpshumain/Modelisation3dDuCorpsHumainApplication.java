package com.example.modelisation3dducorpshumain;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Modelisation3dDuCorpsHumainApplication {

    public static void main(String[] args) {
        SpringApplication.run(Modelisation3dDuCorpsHumainApplication.class, args);
    }

}
